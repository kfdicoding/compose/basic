package learn.dicoding.composebasic.ui.main.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import learn.dicoding.composebasic.ui.helper.BuildThis


@Composable
fun GreetingList(names: List<String>){
    Column (
        verticalArrangement = if (names.isEmpty()) Arrangement.Center else Arrangement.Top,
        horizontalAlignment = if (names.isEmpty()) Alignment.CenterHorizontally else Alignment.Start
    ){
        names.isEmpty().BuildThis{
            Text("No People to great")
        }
        names.map {name->
            Greeting(name)
        }
    }
}

private val sampleName = listOf(
    "Andre",
    "Desta",
    "Parto",
    "Wendy",
    "Komeng",
    "Raffi Ahmad",
    "Andhika Pratama",
    "Vincent Ryan Rompies"
)

@Preview(device = Devices.PIXEL_4)
@Composable
private fun ListNotEmptyPreview(){
    GreetingList(names = sampleName)
}

@Preview(showBackground = true, device = Devices.PIXEL_4)
@Composable
private fun ListEmptyPreview(){
    Surface (modifier = Modifier.fillMaxSize()){
        GreetingList(names = emptyList())
    }
}
