package learn.dicoding.composebasic.ui.helper

import androidx.compose.runtime.Composable

@Composable
fun Boolean.BuildThis(
    job: @Composable () -> Unit
){
    if (this){
        job()
    }
}
